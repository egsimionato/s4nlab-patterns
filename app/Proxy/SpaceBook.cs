using System;
using System.Collections.Generic;

namespace SpaceBookSystem {

    public interface ISpaceBook {
        void Add(string s);
        void Add(string friend, string message);
        void Poke(string who);
    }

// aka: Subject
public class SpaceBook {
    static SortedList <string, SpaceBook> community = 
        new SortedList <string, SpaceBook> (100);

    string pages;
    string name;
    string gap = "\n\t\t\t\t";

    public static bool IsUnique(string name) {
        return community.ContainsKey(name);
    }
    internal SpaceBook(string n) {
        name = n;
        community[n] = this;
    }

    internal void Add(string s) {
        pages += gap+s;
        Console.Write("|--> " + gap+"======== "+name+"'s SpaceBook ========");
        Console.Write("|--> " + pages);
        Console.WriteLine("|--> " + gap+"==================================");
    }

    internal void Add(string friend, string message) {
        community[friend].Add(message);
    }

    internal void Poke(string who, string friend) {
        community[who].pages += gap + friend + " pocked you";
    }
}

public class MySpaceBook : ISpaceBook {
    SpaceBook spaceBook;
    string password;
    string name;
    bool loggedIn = false;

    void Register() {
        Console.WriteLine(" > Let's register you for SpaceBook");
        do {
            Console.WriteLine(" |--> All Spacebook names must be unique");
            Console.Write(" |--> Type in a user name: ");
            name = Console.ReadLine();
        } while (SpaceBook.IsUnique(name));
        Console.Write(" |--> Type in a password: ");
        password = Console.ReadLine();
        Console.WriteLine(" > Thanks for registering with SpaceBook");
    }

    bool Authenticate() {
        Console.Write("Welcome " + name + ". Please type in your password: ");
        string supplied = Console.ReadLine();
        if (supplied == password) {
            loggedIn = true;
            Console.WriteLine("Logged into Spacebook");
            if (spaceBook == null) {
                spaceBook = new SpaceBook(name);
            }
            return true;
        }
        Console.WriteLine("Incorrect password");
        return false;
    }

    public void Add(string message) {
        Check();
        if (loggedIn) {
            spaceBook.Add(message);

        }
    }

    public void Add(string friend, string message) {
        Check();
        if (loggedIn) {
            spaceBook.Add(friend, name + " said: " + message);
        }
    }

    public void Poke(string who) {
        Check();
        if (loggedIn) {
            spaceBook.Poke(who, name);
        }
    }

    public void Check() {
        if (!loggedIn) {
            if (password == null) {
                Register();
            }
            if (spaceBook == null) {
                Authenticate();
            }
        }
    }

}


public class SpaceBookMain {

    public static void xMain(string[] args) {
        Console.WriteLine("> SpaceBook running ... ");
        Console.WriteLine("|> Proxy Pattern \n");


        ISpaceBook me = new MySpaceBook();
        me.Add("Hello World!");
        me.Add("Today I worked 18 hours");

        ISpaceBook tom = new MySpaceBook();
        tom.Add("Off to see the Lion King tonight");

        me.Poke("Tom");
        me.Add("Tom", "Poor you");

        Console.WriteLine("\n\n> Good Bye !!");
    }
}


}
