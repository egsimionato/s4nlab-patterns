using System;

namespace ProxyApp {

    public interface ISubject {
        string Request();
    }
    public class Subject {
        public string Request() {
            return "Subject Request " + "Choose left door\n";
        }
    }
    public class VirtualProxy : ISubject {
        Subject subject;
        public string Request() {
            if (subject == null) {
                Console.WriteLine("|--> Virtual Proxy: Subject inactive ");
                subject = new Subject();
            }
            Console.WriteLine("|--> Virtual Proxy: Subject active ");
            return "Virtual Proxy: Call to " + subject.Request();
        }
    }
    public class ProtectionProxy : ISubject {
        Subject subject;
        string password = "Abracadabra";

        public void Authenticate(string supplied) {
            if (supplied!=password) {
                Console.WriteLine("|--> Protection Proxy: No access ");
            } else {
                subject = new Subject();
                Console.WriteLine("|--> Protection Proxy: Authenticated ");
            }
        }

        public string Request() {
            if (subject==null) {
                return "Protection Proxy: Authenticate first ";
            }
            return "Protection Proxy: Call to " + subject.Request() + " ";
        }
    }
    public class Client {
    }
    public class ProxyMain {

        public static void xMain(string[] args) {
            Console.WriteLine("> ProxyApp running ... ");
            Console.WriteLine("|> Proxy Pattern \n");

            ISubject subject = new VirtualProxy();
            Console.WriteLine("|> 1. Virtual Proxy : "+ subject.Request() + "\n");
            Console.WriteLine("|> 2. ReCall Virtual Proxy : "+ subject.Request() + "\n");

            subject = new ProtectionProxy();
            Console.WriteLine("|> 3. Protection Proxy with incorrect password : "+ subject.Request() + "\n");
            ((ProtectionProxy)subject).Authenticate("Secret");
            Console.WriteLine("|> 4. Protection Proxy with incorrect password : "+ subject.Request() + "\n");
            ((ProtectionProxy)subject).Authenticate("Abracadabra");
            Console.WriteLine("|> 5. Protection Proxy with correct password : "+ subject.Request() + "\n");


            Console.WriteLine("\n\n> Good Bye !!");
        }
    }



}

