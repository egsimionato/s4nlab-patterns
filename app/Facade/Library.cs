using System;

/**
 * Facade Pattern:
 * Sets up a library of three systems, accessed through a
 * Facade of two operations
 * csc /t:library /out:FacadeLib.dll Facade-Library.cs
 */
namespace FacadeLib {
    internal class SubsystemA {
        internal string A1() {
            return "SubsystemA, Method A1\n";
        }
        internal string A2() {
            return "SubsystemA, Method A2\n";
        }
    }
    internal class SubsystemB {
        internal string B1() {
            return "SubsystemB, Method B1\n";
        }
    }
    internal class SubsystemC {
        internal string C1() {
            return "SubsystemC, Method C1\n";
        }
    }

    public static class Facade {
        static SubsystemA a = new SubsystemA();
        static SubsystemB b = new SubsystemB();
        static SubsystemC c = new SubsystemC();

        public static void Operation1() {
            Console.WriteLine("Operation 1\n" +
                    a.A1() +
                    a.A2() +
                    b.B1());
        }

        public static void Operation2() {
            Console.WriteLine("Operation 2\n" +
                    b.B1() +
                    c.C1());
        }
    }
}
