using System;
using FacadeLib;

/**
 * Compile with csc /r:FacadeLib.dll Facade-Main.cs
 */
class Client {
    static void xMain () {
        Facade.Operation1();
        Facade.Operation2();
    }
}
