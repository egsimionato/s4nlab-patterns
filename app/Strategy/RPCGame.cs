/**
 * RPC Game <State Pattern>
 * In single-person game, the player can be in one of four states:
 * - Resting
 * - Attacking
 * - Panicking
 * - Moving
 * The context of the game consists of the state the player is in at present.
 * Event slightly more complex games have other attributes, such as the number
 * of points scored for and against. The Request that can be put through
 * (in terms of the State pattern) are the moves the player can make,
 * which are the ones to which the states must react.
 * Each of four states implements the six actions in different ways.
 */
