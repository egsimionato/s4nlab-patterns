using System;

namespace StrategySample {

public class Context {

    /* context state */
    public const int start = 5;
    public int Counter = 5;

    /* strategy aggregation */
    IStrategy strategy = new StrategyA();

    /* Algorithm invokes a strategy method */
    public int Algorithm() {
        return strategy.Move(this);
    }

    public void SwitchStrategy() {
        if (strategy is StrategyA) {
            strategy = new StrategyB();
        } else {
            strategy = new StrategyA();
        }
    }
}

public interface IStrategy {
    int Move (Context c);
}

public class StrategyA : IStrategy {
    public int Move (Context c) {
        return ++c.Counter;
    }
}

public class StrategyB : IStrategy {
    public int Move (Context c) {
        return --c.Counter;
    }
}


/* aka Client */
public class ClientApp {
    public static void xMain(string[] args){
        Console.WriteLine("[Strategy] Sample App running ...");
        Context context = new Context();
        context.SwitchStrategy();
        Random r = new Random(37);
        Console.Write(" > |_ ");
        for (int i=Context.start; i<=Context.start+15; i++) {
            if (r.Next(3) == 2) {
                Console.Write("_|_ ");
                context.SwitchStrategy();
            }
            Console.Write(context.Algorithm() + " ");
        }
        Console.WriteLine("_|");
        Console.WriteLine("[Strategy] Good bye!");
    }
}


}
