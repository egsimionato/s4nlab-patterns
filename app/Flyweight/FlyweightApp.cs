using System;
using System.IO;
using System.Collections.Generic;

namespace FlyweightPkg {

    public interface IPicture { // aka: IFlyweight
        void Load (string filename);
        void Display (Object group, int row, int col);
    }

    public struct Picture : IPicture { // aka: Flyweight:IFlyweight
        //File pThumbnail;
        public string filename;
        public void Load(string aFilename) {
            this.filename = aFilename;
            Console.WriteLine(" |----> Loading File: " + filename);
        }
        public void Display(Object group, int row, int col) {
            Console.WriteLine(" |----> Display File: " + this.filename + " in [" + row + ":" + col + "] for " + group);
        }
    }

    public class Album { // aka: FlyweightFactory

        Dictionary <string, IPicture> flyweights =
            new Dictionary <string, IPicture>();

        public Album() {
            flyweights.Clear();
        }

        public IPicture this[string index] {
            get {
                if (!flyweights.ContainsKey(index)) {
                    flyweights[index] = new Picture();
                }
                return flyweights[index];
            }
        }
    }

    public class Client {
        static Album album = new Album();

        static Dictionary <string, List<string>> allGroups =
            new Dictionary <string, List<string>> ();

        public void LoadGroups() {
            var myGroups = new[] {
                new {
                    Name = "Garden",
                    Members = new [] {"pot.jpg", "spring.jpg", "flowers.jpg"},
                },
                new {
                    Name = "Italy",
                    Members = new [] {"pasta.jpg", "cappucino.jpg", "lemonade.jpg"},
                },
                new {
                    Name = "Friends",
                    Members = new [] {"restaurant.jpg", "dinner.jpg"}
                }
            };

            foreach (var g in myGroups) {
                allGroups.Add(g.Name, new List<string>());
                foreach (string filename in g.Members) {
                    allGroups[g.Name].Add(filename);
                    album[filename].Load(filename);
                }
            }
        }

        public void DisplayGroups () {
            int row=1;
            foreach(string g in allGroups.Keys) {
                int col=1;
                Console.WriteLine(" |--> " + g + " :");
                foreach (string filename in allGroups[g]) {
                    album[filename].Display(g, row, col);
                    col++;
                }
                row++;
            }
        }
    }

    public class MainApp {
        public static void xMain() {
            Console.WriteLine(" > DecoratorApp running ...");
            Console.WriteLine(" |-> Picture Groups :");
            Client client = new Client();
            client.LoadGroups();
            client.DisplayGroups();
        }
    }
}

