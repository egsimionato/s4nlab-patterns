using System;

namespace BridgeSystem {

    class Abstraction {
        Bridge bridge;
        public Abstraction( Bridge implementation ) {
            bridge = implementation;
        }
        public string Operation() {
            return "Abstraction" + " <<< BRIDGE >>>> " + bridge.OperationImpl();
        }
    }

    interface Bridge {
        string OperationImpl();
    }

    class ImplementationA : Bridge {
        public string OperationImpl() {
            return "ImplementationA";
        }
    }

    class ImplementationB : Bridge {
        public string OperationImpl() {
            return "ImplementationB";
        }
    }

    public class BridgeMain {

        public static void xMain(string[] args) {
            Console.WriteLine(" > BridgeApp running ... ");
            Console.WriteLine(" |> Bridge Pattern \n");

            Console.WriteLine(" |-> " + new Abstraction (new ImplementationA()).Operation());
            Console.WriteLine(" |-> " + new Abstraction (new ImplementationB()).Operation());

        }
    }

}
