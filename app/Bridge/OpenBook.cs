using System;
using System.Collections.Generic;

public class Portal {
    private Bridge bridge;
    public Portal (Bridge aSpaceBook) {
        bridge = aSpaceBook;
    }
    public void Add(string message) {
        bridge.Add(message);
    }
    public void Add(string friend, string message) {
        bridge.Add(friend, message);
    }
    public void Poke(string who) {
        bridge.Poke(who);
    }
}

public interface Bridge {
    void Add(string message);
    void Add(string friend, string message);
    void Poke(string who);
}

public class MyOpenBook : Bridge {
    private static int users;
    private SpaceBook myBook;
    private string name;

    public MyOpenBook(string n) {
        name = n;
        users++;
        myBook = new SpaceBook(name + "-"+ users);
    }
    public void Add(string message) {
        myBook.Add(message);
    }
    public void Add(string friend, string message) {
        myBook.Add(friend, name + " : " + message);
    }
    public void Poke(string who) {
        myBook.Poke(who, name);
    }
}

public static class OpenBookExtensions {
    public static void SuperPoke (this Portal me, string who, string what) {
        me.Add(who, what+" you");
    }
}

public class OpenBookMain {
    public static void xMain() {
        Portal judith = new Portal(new MyOpenBook("Judith"));
        judith.Add("Hello world");
        judith.Add("Today I worked 18 hours");
        Portal tom = new Portal(new MyOpenBook("Tom"));
        tom.Poke("Judith-1");
        tom.SuperPoke("Judith-1", "hugged");
        tom.Add("Judith-1", "Poor you");
        tom.Add("Hey, I'm on OpenBook - it's cool!");
    }
}

//****************************************************//
public interface ISpaceBook {
    void Add(string s);
    void Add(string friend, string message);
    void Poke(string who);
}

// aka: Subject
public class SpaceBook {
    static SortedList <string, SpaceBook> community =
        new SortedList <string, SpaceBook> (100);
    string pages;
    string name;
    string gap = "\n\t\t\t\t";

    public static bool IsUnique(string name) {
        return community.ContainsKey(name);
    }
    internal SpaceBook(string n) {
        name = n;
        community[n] = this;
    }
    internal void Add(string s) {
        pages += gap+s;
        Console.Write("|--> " + gap+"======== "+name+"'s SpaceBook ========");
        Console.Write("|--> " + pages);
        Console.WriteLine("|--> " + gap+"==================================");
    }
    internal void Add(string friend, string message) {
        community[friend].Add(message);
    }
    internal void Poke(string who, string friend) {
        community[who].pages += gap + friend + " pocked you";
    }
}

// aka: Proxy
public class MySpaceBook : ISpaceBook {
    private SpaceBook spaceBook;
    private string password;
    private string name;
    private bool loggedIn = false;

    public void Register() {
        Console.WriteLine(" > Let's register you for SpaceBook");
        do {
            Console.WriteLine(" |--> All Spacebook names must be unique");
            Console.Write(" |--> Type in a user name: ");
            name = Console.ReadLine();
        } while (SpaceBook.IsUnique(name));
        Console.Write(" |--> Type in a password: ");
        password = Console.ReadLine();
        Console.WriteLine(" > Thanks for registering with SpaceBook");
    }
    public bool Authenticate() {
        Console.Write("Welcome " + name + ". Please type in your password: ");
        string supplied = Console.ReadLine();
        if (supplied == password) {
            loggedIn = true;
            Console.WriteLine("Logged into Spacebook");
            if (spaceBook == null) {
                spaceBook = new SpaceBook(name);
            }
            return true;
        }
        Console.WriteLine("Incorrect password");
        return false;
    }
    public void Add(string message) {
        Check();
        if (loggedIn) {
            spaceBook.Add(message);

        }
    }
    public void Add(string friend, string message) {
        Check();
        if (loggedIn) {
            spaceBook.Add(friend, name + " said: " + message);
        }
    }
    public void Poke(string who) {
        Check();
        if (loggedIn) {
            spaceBook.Poke(who, name);
        }
    }
    public void Check() {
        if (!loggedIn) {
            if (password == null) {
                Register();
            }
            if (spaceBook == null) {
                Authenticate();
            }
        }
    }
}
