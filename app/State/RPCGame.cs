/**
 *
 * RPC Game <State Pattern>
 *
 * Simple game where the context changes the state based on user input
 * Has four states, each with six operations.
 *
 * In single-person game, the player can be in one of four states:
 * - Resting
 * - Attacking
 * - Panicking
 * - Moving
 * The context of the game consists of the state the player is in at present.
 * Event slightly more complex games have other attributes, such as the number
 * of points scored for and against. The Request that can be put through
 * (in terms of the State pattern) are the moves the player can make,
 * which are the ones to which the states must react.
 * Each of four states implements the six actions in different ways.
 *
 */
using System;
using System.Collections.Generic;

namespace GameStatePattern {

public class GameApp {

    abstract class IState {
        public virtual string Move(Context context) {return "";}
        public virtual string Attack(Context context) {return "";}
        public virtual string Stop(Context context) {return "";}
        public virtual string Run(Context context) {return "";}
        public virtual string Panic(Context context) {return "";}
        public virtual string CalmDown(Context context) {return "";}
    }

    class RestingState : IState {
        public override string Move(Context context) {
            context.State = new MovingState();
            return "You start moving";
        }
        public override string Attack(Context context) {
            context.State = new AttackingState();
            return "You start attacking the darkness";
        }
        public override string Stop(Context context) {
            return "You are already stopped!";
        }
        public override string Run(Context context) {
            return "You cannot run unless you are moving";
        }
        public override string Panic(Context context) {
            context.State = new PanickingState();
            return "You start Panicking and begin seeing things";
        }
        public override string CalmDown(Context context) {
            return "You are already relaxed";
        }
    }
    class AttackingState : IState {
        public override  string Move(Context context) {
            return "You need to stop attacking first";
        }
        public override  string Attack(Context context) {
            return "You attack the darkness for " +
                (new Random().Next(20) + 1) + " damage";
        }
        public override  string Stop(Context context) {
            context.State = new RestingState();
            return "You are calm down and come to rest";
        }
        public override  string Run(Context context) {
            context.State = new MovingState();
            return "You Run away from the fray";
        }
        public override  string Panic(Context context) {
            context.State = new PanickingState();
            return "You start Panicking and begin seeing things";
        }
        public override  string CalmDown(Context context) {
            context.State = new RestingState();
            return "You fall down and sleep";
        }
    }
    class PanickingState : IState {
        public override  string Move(Context context) {
            return "You move around randomly in a blind panic";
        }
        public override  string Attack(Context context) {
            return "You start attacking the darkness, but keeep on missing";
        }
        public override  string Stop(Context context) {
            context.State = new MovingState();
            return "You are start relaxing, but keep on moving";
        }
        public override  string Run(Context context) {
            return "You run around in your panic";
        }
        public override  string Panic(Context context) {
            return "You are already in a panic";
        }
        public override  string CalmDown(Context context) {
            context.State = new RestingState();
            return "You relax and calm dowm";
        }
    }
    class MovingState : IState {
        public override  string Move(Context context) {
            return "You move around randomly";
        }
        public override  string Attack(Context context) {
            return "You need to stop moving first";
        }
        public override  string Stop(Context context) {
            context.State = new RestingState();
            return "You stand still in a dark room";
        }
        public override  string Run(Context context) {
            return "You run around in circles";
        }
        public override  string Panic(Context context) {
            context.State = new PanickingState();
            return "You start Panicking and begin seeing things";
        }
        public override  string CalmDown(Context context) {
            context.State = new RestingState();
            return "You stand still and relax";
        }
    }



    class Context {
        /* state aggregation */
        public IState State { get; set; }

        public void Request(char c) {
            string result;
            switch (char.ToLower(c)) {
                case 'm' : result = State.Move(this); break;
                case 'a' : result = State.Attack(this); break;
                case 's' : result = State.Stop(this); break;
                case 'r' : result = State.Run(this); break;
                case 'p' : result = State.Panic(this); break;
                case 'c' : result = State.CalmDown(this); break;
                case 'e' : result = "Thank you for playing \"The RPC Game\""; break;
                default : result = "[Error] Error, try again"; break;
            }
            Console.WriteLine(" > "+ result);
        }
    }

    public static void xMain(string[] args) {
        Console.WriteLine(" [Game] Starting Game ...");

        Context context = new Context();
        context.State = new RestingState();

        char command = ' ';
        Console.WriteLine(" > Welcome to \"The State Game\"!");
        Console.WriteLine(" > You are standing here looking relaxed!");
        while (command != 'e') {
            Console.WriteLine("\n > What would you like to do now ? Move, Attack, Stop, Run, Panic, CalmDown, Exit.");
            Console.Write(" [a|s|r|p|c|e] > ");
            string choice;
            do
                choice = Console.ReadLine();
            while (choice==null);
            command = choice[0];
            context.Request(command);
        }
        Console.WriteLine("\n [Game] Good bye !");
    }

}
}
