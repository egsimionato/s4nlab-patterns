using System;

/**
 * Template Method Pattern
 * Shows two versions of the same algorithm
 **/

namespace TemplateMethodSample {

    interface IPrimitives {
        string OperationX();
        string OperationY();
    }

    class Algorithm {
        public void TemplateMethod(IPrimitives a) {
            Console.WriteLine(" > " + a.OperationX() + a.OperationY());
        }
    }

    class ClassA : IPrimitives {
        public string OperationX() {
            return "ClassA:OpX";
        }
        public string OperationY() {
            return "ClassA:OpY";
        }
    }

    class ClassB : IPrimitives {
        public string OperationX() {
            return "ClassB:OpX";
        }
        public string OperationY() {
            return "ClassB:OpY";
        }
    }

    public class ClientApp {
        public static void xMain(string[] args) {
            Console.WriteLine(" [TemplateMethod] Template Method Pattern Sample running ...");
            
            Algorithm m = new Algorithm();
            m.TemplateMethod(new ClassA());
            m.TemplateMethod(new ClassB());

            Console.WriteLine(" [TemplateMethod] Good bye!");
        }
    }
}
