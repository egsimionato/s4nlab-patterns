using System;
using Prime;

namespace PrimeApp {

    class PrimeMain {
        public static void Main(string[] args) {
            Console.WriteLine(" [PrimeGenerator] App running ...");

            PrimeGenerator generator = new PrimeGenerator();

            int[] primesArray = generator.GeneratePrimeNumbers(11);
            DisplayIntArray(primesArray);

            generator.result = new int[0];
            Console.WriteLine(" > UncrossIntegersUpTo(11)");
            generator.UncrossIntegersUpTo(11);
            DisplayBoolArray(generator.crossedOut);
            DisplayIntArray(generator.result);

            Console.WriteLine(" > CrossOutMultiples");
            Console.WriteLine(" > - iterationLimit: {0}", generator.DetermineIterationLimit());
            generator.CrossOutMultiples();
            DisplayBoolArray(generator.crossedOut);
            DisplayIntArray(generator.result);


        }

        public static void DisplayIntArray(int[] primesArray) {
            Console.Write("{0}=[", primesArray.Length);
            for (int i = 0; i < primesArray.Length; i++) {
                Console.Write("{0},", i, primesArray[i]);
            }
            Console.WriteLine("]");
        }

        public static void DisplayBoolArray(bool[] primesArray) {
            for (int i = 0; i < primesArray.Length; i++) {
                Console.Write("{0}:", (primesArray[i]?1:0));
            }
            Console.WriteLine("");
        }


    }

}

