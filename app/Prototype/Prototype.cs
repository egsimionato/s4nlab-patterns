using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

/**
 * Prototype Pattern:
 * Serialization is used for the deep copy option.
 * The type T must be marked with the attribute [Serializable()]
 */
namespace PrototypePattern {

[DataContract]
[Serializable()]
public abstract class IPrototype<T> {

    public T Clone() { // Shallow copy
        return (T) this.MemberwiseClone();
    }

    public T DeepCopy() {
       MemoryStream stream = new MemoryStream();
       var serializer = new DataContractJsonSerializer(typeof(T));
       serializer.WriteObject(stream, this);
       stream.Position = 0;
       T copy = (T) serializer.ReadObject(stream);
       //stream.Close();
       return copy;
    }

};


[Serializable()]
public class Box {
    private Point origin {get; set;}
    private string material {get; set;}
    private int height {get; set;}
    private int width {get; set;}
}

[DataContract]
[Serializable()]
public class Point {
    [DataMember]
    private int x;
    [DataMember]
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public override string ToString() {
            return "["+ x +":"+ y +"]";
    }
}
}
