using System;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Collections.Generic;

using PrototypePattern;

/**
 * Prototype Pattern:
 * Serialization is used for the deep copy option.
 * The type T must be marked with the attribute [Serializable()]
 */
namespace PrototypeAppPkg {

/**
 * Helper class used to create a sendonc level data structure
 */
[DataContract]
[Serializable()]
public class DeeperData {
    [DataMember]
    public string data {get; set;}
    public DeeperData(string s) {
        data = s;
    }
    public override string ToString() {
        return data;
    }
}

[DataContract]
[Serializable()]
public class Prototype : IPrototype<Prototype> {
    [DataMember]
    public string country {get; set;}
    [DataMember]
    public string capital {get; set;}
    [DataMember]
    public DeeperData language {get; set;}
    public Prototype(string aCountry, string aCapital, string aLanguage) {
        country = aCountry;
        capital = aCapital;
        language = new DeeperData(aLanguage);
    }
    public override string ToString() {
        return country+"\t\t"+capital+"\t\t->"+language;
    }
};

[Serializable()]
public class PrototypeManager : IPrototype<Prototype> {
    public Dictionary <string, Prototype> prototypes = new Dictionary<string, Prototype>
        {
            {"Germany", new Prototype("Germany", "Berlin", "German")},
            {"ITaly", new Prototype("Italy", "Rome", "ITalian")},
            {"Australia", new Prototype("Australia", "Canberra", "English")}
        };

};


public class PrototypeClient {

    static void Report(string s, Prototype a, Prototype b) {
        Console.WriteLine(s);
        Console.WriteLine("Prototype: " + a + "\n Clone:    " + b);
    }

    public static void xMain(string[] args) {
        Console.WriteLine("\n > Prototype running ...\n");

        PrototypeManager manager = new PrototypeManager();
        Prototype c2, c3;

        c2 = manager.prototypes["Australia"].Clone(); // Australia Shallow Copy
        Report("Shallow cloning Australia\n==========================",
                manager.prototypes["Australia"], c2);
        c2.capital = "Sydney";
        Report("> Altered Clone's shallow state, prototype unaffected",
                manager.prototypes["Australia"], c2);
        c2.language.data = "Chinese";
        Report("> Altering Clone deep state: prototype affected *****",
                manager.prototypes["Australia"], c2);

        c3 = manager.prototypes["Germany"].DeepCopy(); // Germany DeepCopy
        Report("Deep cloning Germany\n=========================",
                manager.prototypes["Germany"], c3);
        c3.capital = "Munich";
        Report("Altering Clone shallow state, prototype unaffected",
                manager.prototypes["Germany"], c3);
        c3.language.data = "Turkish";
        Report("Altering Clone deep state, prototype unaffected",
                manager.prototypes["Germany"], c3);


        Console.WriteLine("\n > Good bye !");
    }
}


}
