using System;
using System.IO;

namespace Examples {

    class IOFileMain  {

        public static void xMain() {

            string path = @"data.txt";

            try {
                if (File.Exists(path)) {
                    File.Delete(path);
                }

                using (FileStream fs = File.Open(path, FileMode.OpenOrCreate)) {
                using (StreamWriter sw = new StreamWriter(fs)) {
                    sw.WriteLine(@"Garden");
                    sw.WriteLine(@"======");
                    sw.WriteLine(@"    - Flowers");
                    sw.WriteLine(@"    - Trees");
                    sw.WriteLine(@"    - Spring");
                    sw.WriteLine(@"Pets");
                    sw.WriteLine(@"====");
                    sw.WriteLine(@"    > Dog");
                    sw.WriteLine(@"    > Cat");
                }
                }

                using (FileStream fs = new FileStream(path, FileMode.Open)) {
                    using (StreamReader sr = new StreamReader(fs)) {
                        while (sr.Peek() >= 0) {
                            Console.WriteLine(sr.ReadLine());
                        }
                    }
                }
            } catch (Exception e) {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
        }
    }
}
