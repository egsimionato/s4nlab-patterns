using System;

namespace BagSystem {

    public interface IBag { // IProductA
        string Material { get; }
    }

    public interface IShoes { // IProductB
        int Price { get; }
    }

    public interface IFactory<Brand> where Brand : IBrand {
        IBag CreateBag();
        IShoes CreateShoes();
    }

    public interface IBrand {
        int Price { get; }
        string Material { get; }
    }

    class Gucci:IBrand {
        public int Price { get { return 1000; } }
        public string Material { get { return "Crocodile skin"; } }
    }

    class Poochy:IBrand {
        public int Price { get { return new Gucci().Price / 3; } }
        public string Material { get { return "Plastic"; } }
    }

    class Groundcover:IBrand {
        public int Price { get { return 2000; } }
        public string Material { get { return "South african leather"; } }
    }

    class Factory<Brand>:IFactory<Brand> where Brand:IBrand, new() {
        public IBag CreateBag() {
            return new Bag<Brand>();
        }
        public IShoes CreateShoes() {
            return new Shoes<Brand>();
        }
    }

    class Bag<Brand>:IBag where Brand:IBrand, new() {
        private Brand myBrand;
        public Bag() {
            myBrand = new Brand();
        }
        public string Material {
            get { return myBrand.Material; }
        }
    }

    class Shoes<Brand>:IShoes where Brand:IBrand, new() {
        private Brand myBrand;
        public Shoes() {
            myBrand = new Brand();
        }
        public int Price {
            get { return myBrand.Price; }
        }
    }

    public class Client<Brand> where Brand:IBrand, new() {

        public void ClientMain() {
            IFactory<Brand> factory = new Factory<Brand>();
            IBag bag = factory.CreateBag();
            IShoes shoes = factory.CreateShoes();

            Console.WriteLine("I bought a Bag which is made from \"" + bag.Material + "\"");
            Console.WriteLine("I bought some shoes which cost " + shoes.Price);
        }
    }


    public class ClientApp {
        static void xMain(string[] args) {

            Console.WriteLine("[AbstractFactory] Bag System running ...");
            Console.WriteLine("\n=====================================");
            new Client<Poochy>().ClientMain();
            Console.WriteLine("=====================================");
            new Client<Gucci>().ClientMain();
            Console.WriteLine("=====================================");
            new Client<Groundcover>().ClientMain();
            Console.WriteLine("=====================================\n");
            Console.WriteLine("[AbstractFactory] Good bye!!");
        }
    }

}

