using System;

namespace DecoratorApp {

    public interface IComponent {
        string Operation();
    }

    class Component : IComponent {

        public string Operation() {
            return "I am walking ";
        }
    }

    class DecoratorA : IComponent {
        IComponent component;
        public DecoratorA(IComponent c) {
            component = c;
        }
        public string Operation() {
            string s = component.Operation();
            s += "and Listing to Classic FM ";
            return s;
        }
    }

    class DecoratorB : IComponent {
        IComponent component;
        public string addedState = "past the Coffee Shop ";
        public DecoratorB (IComponent c) {
            component = c;
        }
        public string Operation() {
            string s = component.Operation();
            s += "to school ";
            return s;
        }
        public string AddedBehavior() {
            return "and I bought a cappuccino ";
        }
    }

    class Client {
        public static void Display(string s, IComponent c) {
            Console.WriteLine(s + c.Operation());
        }
    }

    public class DecoratorMain {

        public static void xMain(string[] args) {
            Console.WriteLine("> DecoratorApp running ...");
            Console.WriteLine("| > Decorator Pattern\n");

            IComponent component = new Component();
            Client.Display("| > 1. Basic component : ", component);
            Client.Display("| > 2. A-decorated : ", new DecoratorA(component));
            Client.Display("| > 3. B-decorated : ", new DecoratorB(component));
            Client.Display("| > 4. B-A-decorated : ", new DecoratorB(new DecoratorA(component)));
            DecoratorB b = new DecoratorB(new Component());
            Client.Display("| > 5. A-B-decorated : ", new DecoratorA(b));
            Console.WriteLine("\t\t\t" + b.addedState + b.AddedBehavior());


            Console.WriteLine("\n\n> Good Bye !!");
        }
    }

}
