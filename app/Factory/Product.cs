using System;
using System.Collections;

namespace FactoryPck {

    public interface IProduct {
        string ShipFrom();
    }

    public class ProductA : IProduct {
        public string ShipFrom() {
            return " from South Africa";
        }
    }

    public class ProductB : IProduct {
        public string ShipFrom() {
            return " from Spain";
        }
    }

    class DefaultProduct : IProduct {
        public string ShipFrom() {
            return "not available";
        }
    }

    class Creator { // Factory
        public IProduct FactoryMethod(int month) {
            if (month >= 4 && month <= 11) {
                return new ProductA();
            } else if (month == 1 || month == 2 || month == 12) {
                return new ProductB();
            } else {
                return new DefaultProduct();
            }
        }
    }

    public class MainClient {
        public static void xMain(string[] args) {

            Console.WriteLine("\n> Factory running...");
            Creator c = new Creator();
            IProduct product;

            for (int i=1; i<=12; i++) {
                product = c.FactoryMethod(i);
                Console.WriteLine("[{0:00}]: Avocados {1}", i, product.ShipFrom());
            }

            Console.WriteLine("> Good bye !");
        }
    }
}
