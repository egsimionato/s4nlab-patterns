using System;
using System.Collections;

using SingletonPattern;

namespace EarlySingleton {

    public sealed class Singleton {
        static readonly Singleton instance = new Singleton();
        public static Singleton Instance {
            get { return instance; }
        }

        Singleton() {} // private constructor
    }

}

namespace LazySingleton {

    public class Singleton {
        class SingletonCreator {
            static SingletonCreator() {}
            internal static readonly Singleton instance = new Singleton();
        }
        public static Singleton Instance {
            get {return SingletonCreator.instance;}
        }
        Singleton() {}
    }
}


namespace SingletonSample {

    public class Client {
        static void xMain(string[] args) {
            Point p1a = Singleton<Point>.Instance;
            Point p1b = Singleton<Point>.Instance;
            Vector v1a = Singleton<Vector>.Instance;
            Vector v1b = Singleton<Vector>.Instance;

            Console.WriteLine(" > p1a: {0}", p1a.ToString());
            Console.WriteLine(" > p1b: {0}", p1b.ToString());
            p1a.x = 2;
            p1a.y = 4;
            Console.WriteLine(" > p1a: {0}", p1a.ToString());
            Console.WriteLine(" > p1b: {0}", p1b.ToString());

            Console.WriteLine(" > v1a: {0}", v1a.ToString());
            Console.WriteLine(" > v1b: {0}", v1b.ToString());
            v1a.origin.x = 8;
            v1a.origin.y = 5;
            v1a.magnitude.dx = -3;
            v1a.magnitude.dy = -9;
            Console.WriteLine(" > v1a: {0}", v1a.ToString());
            Console.WriteLine(" > v1b: {0}", v1b.ToString());

        }
    }
}
