using System;

namespace SingletonPattern {

    public class Singleton <T> where T : class, new () {
        Singleton() {}
        class SingletonCreator {
            static SingletonCreator() {}
            internal static readonly T instance = new T();
        }

        public static T Instance {
            get {return SingletonCreator.instance;}
        }
    }

    public class Point {
        public int x {get; set;}
        public int y {get; set;}
        public Point() {
            x = 0;
            y = 0;
        }
        public Point(int ax, int ay) {
            x = ax;
            y = ay;
        }
        public override string ToString() {
                return "("+ x +":"+ y +")";
        }
    }

    public class Magnitude {
        public int dx {get; set;}
        public int dy {get; set;}
        public Magnitude() {
            dx = 1;
            dy = 1;
        }
        public Magnitude(int adx, int ady) {
            dx = adx;
            dy = ady;
        }
        public override string ToString() {
            return "[ dx=> "+ dx +", dy=> "+ dy +" ]";
        }
    }

    public class Vector {
        public Point origin {get; set;}
        public Magnitude magnitude {get; set;}
        public Vector () {
            origin = new Point(0,0);
            magnitude = new Magnitude(1,1);
        }
        public override string ToString() {
            return "{origin: "+ origin.ToString()+" and magnitude: "+ magnitude.ToString()+"}";
        }

    }

}
