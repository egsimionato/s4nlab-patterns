using System;

namespace ChainAndStateSample {

    class Handler {

        Handler next;
        int id;
        public int Limit {get; set;}

        public Handler (int id, Handler handler) {
            this.id = id;
            Limit = id*1000;
            next = handler;
        }
        public string HandleRequest(int data) {
            if (data < Limit) {
                return " > Request for " + data + " handled at level " + id;
            } else if (next != null) {
                return next.HandleRequest(data);
            } {
                return (" > Request for " + data + " handle By Default at level " + id);
            }
        }
    }

    public class ClientApp {
        public static void xMain(string[] args) {
            Console.WriteLine("\n [ChainOfResponsability] ChainOfResponsability and State Pattern Sample running ...\n");

            Handler start = null;
            for (int i=5; i>0; i--) {
                Console.WriteLine(" > Handler " + i + " deals up to a limit of " + (i * 1000));
                start = new Handler(i, start);
            }
            Console.WriteLine();

            int[] a = {50, 2000, 1500, 10000, 175, 4500};
            foreach (int i in a) {
                Console.WriteLine(start.HandleRequest(i));
            }

            Console.WriteLine("\n [ChainOfResponsability] Good bye!");
        }
    }

}
