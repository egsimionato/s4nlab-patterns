using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace BagBuilderSystem {

public interface IBuilder<Brand>
        where Brand : IBrand {
    IBag CreateBag();
}

public class Builder<Brand> : IBuilder<Brand>
        where Brand:IBrand, new() {
    Brand myBrand;
    public Builder() {
        myBrand = new Brand();
    }

    public IBag CreateBag() {
        return myBrand.CreateBag();
    }
}

public interface IBag {
    string Properties { get; set; }
}

public class Bag : IBag {
    public string Properties { get; set; }
}

public interface IBrand {
    IBag CreateBag();
}

public class Gucci : IBrand {
    public IBag CreateBag() {
        Bag b = new Bag();
        Program.DoWork("Cut Leather", 250);
        Program.DoWork("Sew Leather", 1000);
        b.Properties += "Leather";
        Program.DoWork("Create Lining", 500);
        Program.DoWork("Attach Lining", 1000);
        b.Properties += " lined";
        Program.DoWork("Add Label", 250);
        b.Properties += " with label";
        return b;
    }
}

public class Poochy : IBrand {
    public IBag CreateBag() {
        Bag b = new Bag();
        Program.DoWork("Hire cheap labour", 200);
        Program.DoWork("Cut Plastic", 125);
        Program.DoWork("Sew Plastic", 500);
        b.Properties += "Plastic";
        Program.DoWork("Add Label", 100);
        b.Properties += " with label";
        return b;
    }
}

public class Client<Brand>
    where Brand : IBrand, new() {
    
    public void ClientMain() { //IFactory<Brand> factory
        IBuilder<Brand> factory = new Builder<Brand>();
        DateTime date = DateTime.Now;
        Console.WriteLine("I want to buy a bag!");
        IBag bag = factory.CreateBag();
        Console.WriteLine("I got my Bag wich took " +
                DateTime.Now.Subtract(date).TotalSeconds * 5 + " days");
        Console.WriteLine("\t with the following properties: " + bag.Properties);

    }
}

public class Program {
    public static void DoWork(string workItem, int time) {
        Console.Write("" + workItem + ": 0%");
        Thread.Sleep(time);
        Console.Write("....25%");
        Thread.Sleep(time);
        Console.Write("....50%");
        Thread.Sleep(time);
        Console.Write("....75%");
        Thread.Sleep(time);
        Console.WriteLine("....100%");
    }
}

public class ClientApp {
    public static void xMain(string[] args){
        Console.WriteLine("[Builder] Bag Builder System running ...");

        new Client<Poochy>().ClientMain();
        new Client<Gucci>().ClientMain();

        Console.WriteLine("[Builder] Good Bye !");
    }
}

}
