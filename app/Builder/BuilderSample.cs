using System;
using System.Collections.Generic;

public class Director {
    public void Construct(IBuilder builder) {
        builder.BuildPart1();
        builder.BuildPart2();
        builder.BuildPart2();
    }
}

public interface IBuilder {
    void BuildPart1();
    void BuildPart2();
    Product GetResult();
}

public class SimpleBuilder : IBuilder {
    private Product product = new Product();
    public void BuildPart1() {
        product.Add("PartA");
    }
    public void BuildPart2() {
        product.Add("PartB");
    }
    public Product GetResult() {
        return product;
    }
}

public class InverseBuilder : IBuilder {
    private Product product = new Product();
    public void BuildPart1() {
        product.Add("PartX");
    }
    public void BuildPart2() {
        product.Add("PartY");
    }
    public Product GetResult() {
        return product;
    }
}

public class Product {
    List <string> parts = new List<string>();
    public void Add(string part) {
        parts.Add(part);
    }
    public void Display() {
        Console.Write(" > Product Parts: ");
        foreach (string part in parts) {
            Console.Write(part+", ");
        }
        Console.WriteLine("|");
    }
}

public class ClientApp {
    static void xMain(string[] args) {
        Console.WriteLine("[Builder] Sample System running ...");

        Director director = new Director();
        IBuilder b1 = new SimpleBuilder();
        IBuilder b2 = new InverseBuilder();

        director.Construct(b1);
        Product p1 = b1.GetResult();
        p1.Display();

        director.Construct(b2);
        Product p2 = b2.GetResult();
        p2.Display();

        Console.WriteLine("[Builder] Good Bye!!");
    }
}
