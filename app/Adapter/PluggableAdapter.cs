using System;

/**
 * Adapter Pattern:
 * Adapter can accept any number of pluggable adaptees and targets
 * and route the requests via a delegate, in some cases using the
 * anonymous delegate construct
 */
namespace PluggableAdapterPkg {

    /**
     * Existing way requests are implment
     */
    public class Adaptee {
        public double Precise (double a, double b) {
            return a/b;
        }
    }

    /**
     * New standard for requests
     */
    public class Target {
        public string Estimate(int i) {
            return "Estimate is "+ (int) Math.Round(i/3.0);
        }
    }

    /**
     * Implementing new requests via old
     */
    public class Adapter : Adaptee {
        public Func<int,string> Request;

        public Adapter(Adaptee adaptee) {
            Request = delegate(int i) { // set the delegate to the new standard
                return "Estimate based on precision is " +
                    (int) Math.Round(adaptee.Precise(i,4));
            };
        }
        public Adapter(Target target) {
            Request = target.Estimate;
        }

    }



    public class ClientApp {
        public static void xMain(string[] args) {
            Console.WriteLine(" > PluggableAdapter running ...\n");

            Adapter adap1 = new Adapter(new Adaptee());
            Console.WriteLine(adap1.Request(5));

            Adapter adap2 = new Adapter(new Target());
            Console.WriteLine(adap2.Request(5));

            //Console.ReadLine();
            Console.WriteLine("\n > Good bye!!");

        }
    }

}
