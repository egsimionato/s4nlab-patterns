using System;

namespace AdapterPkg {

    class Adaptee {
        public double SpecificRequest(double a, double b) {
            return a/b;
        }
    }

    public interface ITarget {
        string Request(int i);
    }

    class Adapter : Adaptee, ITarget {
        public string Request (int i) {
            return "Rough estimate is " + (int) Math.Round(SpecificRequest(i, 3));
        }
    }

    public class App {
        public static void xMain(String[] args) {
            Console.WriteLine("\n > Adapter App running ...\n");

            Adaptee first = new Adaptee();
            Console.WriteLine(" |> Before the new standard");
            Console.WriteLine(" |> Precise reading");
            Console.WriteLine(" |> " + first.SpecificRequest(5,3));
            ITarget second = new Adapter();
            Console.WriteLine("\n |> Moving to the new standard");
            Console.WriteLine(second.Request(5));

            Console.WriteLine("\n\n > Good Bye !");
        }
    }

}
