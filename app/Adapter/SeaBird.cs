using System;

namespace SeaBirdFlyPkg {

public interface IAircraft { //ITarget Interface
    bool Airborne {get;}
    void TakeOff();
    int Height {get;}
}

public class Aircraft : IAircraft { // Target Implement
    int height;
    bool airborne;
    public Aircraft() {
        height = 0;
        airborne = false;
    }
    public void TakeOff() {
        Console.WriteLine("Aircraft engine takeoff");
        airborne = true;
        height = 200; // Meters
    }
    public bool Airborne {
        get {return airborne;}
    }
    public int Height {
        get {return height;}
    }
}

public interface ISeacraft { // Adaptee Interface
    int Speed {get;}
    void IncreaseRevs();
}

public class Seacraft : ISeacraft { // Adaptee Implement
    int speed = 0;
    public virtual void IncreaseRevs() {
        speed += 10;
        Console.WriteLine(
                "Seacraft engine increases revs to " + speed + " knots");
    }
    public int Speed {
        get {return speed;}
    }
}

public class Seabird : Seacraft, IAircraft { // Adapter
    int height = 0;
    /*
     * A two-way adapter hides and routes the Target's methods
     * Use Seacraft instructions to implement this one
    */
    public void TakeOff() {
        while (!Airborne) {
            IncreaseRevs();
        }
    }
    /*
     * Routes this straight back to the Aircraft
     */
    public int Height {
        get {return height;}
    }
    /*
     * This method is common to both Target and Adaptee
     */
    public override void IncreaseRevs() {
        base.IncreaseRevs();
        if (Speed > 40) {
            height += 100;
        }
    }
    public bool Airborne {
        get {return height > 50;}
    }
}

public class MakeSeaBirdFly {
    public static void xMain(String[] args) {

        Console.WriteLine("\n > SeaBirdFly App running ...\n");

        Console.WriteLine(" > Experiment #1: test the aircraft engine");
        IAircraft aircraft = new Aircraft();
        aircraft.TakeOff();
        if (aircraft.Airborne) {
            Console.WriteLine(" #1 > The aircraft engine is fine, flying at " +
                    aircraft.Height + " meters");
        }

        Console.WriteLine(" > Experiment #2: Use the engine in the Seabird");
        IAircraft seabird = new Seabird();
        seabird.TakeOff(); // And automatically increases speed
        Console.WriteLine(" #2 > The Seabrid took off");

        //Console.ReadLine();
        Console.WriteLine("\n > Good Bye!!");
    }
}

}
