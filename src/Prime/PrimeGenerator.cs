/**
 * Prime Generator
 *
 * Generates prime numers up to a user specified maximun.
 *
 * The algorithm used is the "Sieve of Eratosthenes".
 * Given an array of integers starting at 2:
 * Find the first uncrossed integer, and cross out all its
 * multiples. Repeat until there are no more multiples in 
 * the array.
 */

using System;

namespace Prime {

    public class PrimeGenerator {

        public bool[] crossedOut;
        public int[] result;

        public int[] GeneratePrimeNumbers(int maxValue) {
            if (maxValue < 2) {
                return new int[0];
            } else {
                UncrossIntegersUpTo(maxValue);
                CrossOutMultiples();
                PutUncrossedIntegersIntoResult();
                return result;
            }
        }

        public void UncrossIntegersUpTo(int maxValue) {
            crossedOut = new bool[maxValue + 1];
            for (int i = 2; i < crossedOut.Length; i++) {
                crossedOut[i] = false;
            }
        }

        public void CrossOutMultiples() {
            int limit = DetermineIterationLimit();
            for (int i = 2; i <= limit; i++) {
                if(NotCrossed(i)) {
                    CrossOutMultiplesOf(i);
                }
            }
        }

        /**
         * Every multiple in the array has a prime factor that
         * is less than or equal to the root of the array size,
         * so we don't have to cross off multiples of numbers
         * larger than that root.
         */
        public int DetermineIterationLimit() {
            double iterationLimit = Math.Sqrt(crossedOut.Length);
            return (int) iterationLimit;
        }

        public bool NotCrossed(int i) {
            return crossedOut[i] == false;
        }

        public void CrossOutMultiplesOf(int i) {
            for (int multiple = 2*i;
                    multiple < crossedOut.Length;
                    multiple += i) {
                crossedOut[multiple] = true;
            }
        }


        public void PutUncrossedIntegersIntoResult() {
            result = new int[NumberOfUncrossedIntegers()];
            for (int j = 0, i = 2; i < crossedOut.Length; i++) {
                if (NotCrossed(i)) {
                    result[j++] = i;
                }
            }
        }

        public int NumberOfUncrossedIntegers() {
            int count = 0;
            for (int i = 2; i < crossedOut.Length; i++) {
                if (NotCrossed(i)) {
                    count++; /* bump count */
                }
            }
            return count;
        }


    }
}

