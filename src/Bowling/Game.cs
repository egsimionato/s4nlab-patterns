//using System;
//
//namespace Bowling {
//
//    /**
//     * Game keeps tracks of frames.
//     */
//    public class Game {
//        private string title; // {get; set;}
//        private int id; // {get; set;}
//
//        private int score;
//        private int currentFrame = 1;
//        private bool isFirstThrow = true;
//        private Scorer scorer = new Scorer();
//
//        public void Add(int pins) {
//            scorer.AddThrow(pins);
//            score += pins;
//            AdjustCurrentFrame(pins);
//        }
//
//        private void AdjustCurrentFrame(int pins) {
//            if (IsStrike(pins) || (!isFirstThrow)) {
//                AdvanceFrame();
//            } else {
//                isFirstThrow = false;
//            }
//          //  if (isFirstThrow) {
//          //      if (IsStrike(pins)) { // Strike
//          //          AdvanceFrame();
//          //      } else {
//          //          isFirstThrow = false;
//          //      }
//          //  } else {
//          //      isFirstThrow = true;
//          //      AdvanceFrame();
//          //  }
//        }
//
//        private bool IsStrike(int pins) {
//            return (isFirstThrow && pins == 10);
//        }
//
//        private void AdvanceFrame() {
//            currentFrame++;
//            /* limit frame to 10 frames */
//            if (currentFrame > 10) {
//                currentFrame = 10;
//            }
//        }
//
//        public int ScoreForFrame(int theFrame) {
//            return scorer.ScoreForFrame(theFrame);
//        }
//
//
//        /* getters and setters */
//        public int Score {
//            //get { return score; }
//            //get { return ScoreForFrame(currentFrame - 1); }
//            //get { return ScoreForFrame(currentFrame); }
//            get { return ScoreForFrame(currentFrame); }
//        }
//        public int CurrentFrame {
//            //get { return 1 + (currentThrow -1) / 2; }
//            get { return currentFrame; }
//        }
//        public int CurrentThrow {
//            get { return scorer.CurrentThrow; }
//        }
//    }
//
//    /**
//     * Scorer calculates the score.
//     */
//    class Scorer {
//        private int ball;
//        private int[] throws = new int[21];
//        private int currentThrow;
//
//
//        public void AddThrow(int pins) {
//            throws[currentThrow++] = pins;
//        }
//
//        public int ScoreForFrame(int theFrame) {
//            ball = 0;
//            int score = 0;
//            for (int currentFrame = 0;
//                    currentFrame < theFrame;
//                    currentFrame++) {
//
//                if (IsStrike()) {
//                    score += 10 + NextTwoBallsForStrike;
//                    ball++;
//                } else if (IsSpare()) {
//                    score += 10 + NextBallForSpare;
//                    ball += 2;
//                } else {
//                    score += NextTwoBalls;
//                    ball += 2;
//                }
//            }
//            return score;
//        }
//
//        /* private functions */
//        private int NextTwoBallsForStrike {
//            get { return (throws[ball+1] + throws[ball+2]); }
//        }
//        private int NextBallForSpare {
//            get { return throws[ball+2]; }
//        }
//        private bool IsStrike() {
//            return throws[ball] == 10;
//        }
//        private bool IsSpare() {
//            return throws[ball] + throws[ball+1] == 10;
//        }
//        private int NextBall {
//            get { return throws[ball]; }
//        }
//        private int NextTwoBalls {
//            get { return (throws[ball] + throws[ball+1]); }
//        }
//
//        /* getter and setters */
//        public int CurrentThrow {
//            get { return currentThrow; }
//        }
//
//    }
//
//}
