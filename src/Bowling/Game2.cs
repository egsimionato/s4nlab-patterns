using System;

/**
 *
 * Bowling Rules:
 *   Bowling is a game that is played by throwing a cantaloupe-sized ball down
 * a narrow alley toward ten wooden pins.
 * The object is to knock down as many pins as possible per throw.
 *   The game is played in ten frames. At the beginning of each frame, all ten
 * pins are set up. The player then gets two tries to knock them all down.
 *   If the player knocks all the pins down on the first try, it is called
 * a "strike", and the frame ends. If the player fails to knock down all the
 * pins with the first ball but succeeds with the second ball, it is called a
 * "spare". After the second ball of the frame, the frame ends even if pins
 * are still standing.
 *   A Strike frame is scored by adding ten, plus the number of pins
 * knocked down by the next two balls, to the score of the previous frame.
 * A Spare frame is scored by adding ten, plus the number of pins knocked down
 * by the next ball, to the score of the previous frame. Otherwise, a frame is
 * scored by adding the numer of pins knocked down by the two balls in the frame
 * to the score of the previous frame.
 *   If a strike is thrown in the tenth frame, the player may throw two more balls
 * to complete the score of the strike. Likewise, if a spare is thrown in the tenth
 * frame, the player may throw one more ball to complete the score of the spare.
 * Thus, the tenth frame may have three balls instead of two.
 *
 * [(1:4);5],[(4:5);14],[(6:4*);29],[(5:5*);49],[(10**);60],
 * [(0:1);61],[(7:3*);77],[(6:4*);97],[(10**);117],[(2:8**);133]{6}
 * In the first frame, score for the frame is 5.
 * In the second frame, knocked down four pins with the first ball, and five more
 * with the second.
 * In the third frame, the player knocked down six pins with the first ball and
 * down the rest with the second for a "spare". No score can be calculated for
 * this frame until the next ball is rolled.
 * In the fourth frame, the player knocks down five pins with the first ball.
 * This lets us complete the scoring of the spare in frame 3. The score for
 * frame 3: 10 plus the score in the frame 2 (14) plus the first ball of
 * frame 4 (5), or 29. The final ball of frame 4 is a spare.
 * Frame 5 is a strike. This lets us finish the score of frame 4 with is
 * 29 + 10 + 10 = 49.
 * Frame 6 is dismal. The first ball went in the gutter and failed to knock
 * down any pins. The second ball knocked down only one pin. The score for the
 * strike in frame 5 is 49 + 10 + 0 + 1 = 60.
 * ...
 */
namespace Bowling {

    public class Game {

        private int currentFrame = 0;
        private bool isFirstThrow = true;
        private Scorer scorer = new Scorer();

        public void Add(int pins) {
            scorer.AddThrow(pins);
            AdjustCurrentFrame(pins);
        }

        private void AdjustCurrentFrame(int pins) {
            if (LastBallInFrame(pins)) {
                AdvanceFrame();
            } else {
                isFirstThrow = false;
            }
        }

        private bool LastBallInFrame(int pins) {
            return Strike(pins) || (!isFirstThrow);
        }

        private bool Strike(int pins) {
            return (isFirstThrow && pins == 10);
        }

        private void AdvanceFrame() {
            currentFrame++;
            if (currentFrame > 10) {
                currentFrame = 10;
            }
        }

        public int ScoreForFrame(int theFrame) {
            return scorer.ScoreForFrame(theFrame);
        }


        /* Getters */
        public int Score {
            get { return ScoreForFrame(currentFrame); }
        }
        public int CurrentFrame {
            get { return currentFrame; }
        }
    }

    public class Scorer {

        private int ball;
        private int[] throws = new int[21];
        private int currentThrow;

        public void AddThrow(int pins) {
            throws[currentThrow++] = pins;
        }

        public int ScoreForFrame(int theFrame) {
            ball = 0;
            int score = 0;
            for (int currentFrame = 0;
                    currentFrame < theFrame;
                    currentFrame++) {

                if (Strike()) {
                    score += 10 + NextTwoBallsForStrike;
                    ball++;
                } else if (Spare()) {
                    score += 10 + NextBallForSpare;
                    ball += 2;
                } else {
                    score += TwoBallsInFrame;
                    ball += 2;
                }
            }
            return score;
        }


        private int TwoBallsInFrame {
            get { return throws[ball] + throws[ball+1]; }
        }

        private bool Strike() {
            return throws[ball] == 10;
        }

        private bool Spare() {
            return throws[ball] + throws[ball+1] == 10;
        }

        private int NextTwoBallsForStrike {
            get { return (throws[ball+1] + throws[ball+2]); }
        }

        private int NextBallForSpare {
            get { return throws[ball+2]; }
        }
    }

}
